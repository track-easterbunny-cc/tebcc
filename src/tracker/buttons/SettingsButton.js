import { MDBBtn, MDBModal, MDBModalContent, MDBModalDialog, MDBModalHeader, MDBModalTitle, MDBModalBody, MDBModalFooter, MDBRow, MDBTabsContent, MDBTabsPane, MDBListGroup, MDBListGroupItem, MDBRipple } from "mdb-react-ui-kit";
import SettingsIcon from '@mui/icons-material/Settings'
import TrackChangesIcon from '@mui/icons-material/TrackChanges'
import MapIcon from '@mui/icons-material/Map'
import ShowChartIcon from '@mui/icons-material/ShowChart'
import RefreshIcon from '@mui/icons-material/Refresh'
import { HelpOutline, InfoOutlined } from "@mui/icons-material";
import LanguageIcon from '@mui/icons-material/Language'
import './Buttons.css'

import React, { useEffect, useState } from "react";
import TrackerSettings from "./settings/Tracker";
import MapSettings from "./settings/Map";
import MetricsSettings from "./settings/Metrics";
import ResetSettings from "./settings/Reset";
import HelpSettings from "./settings/Help";
import LocalizationSettings from "./settings/Localization";
import StopInfoSettings from "./settings/StopInfo";

export default function SettingsButton(props) {
    const [settingsModal, setSettingsModal] = useState(false);
    const [activeSettingsTab, setActiveSettingsTab] = useState('tracker');
    const [settingsMgr, setSettingsMgr] = useState(props.settingsMgr)
    const [activeAppearance, setActiveAppearance] = useState(settingsMgr.get("appearance_actual"))
    const [geoMetricsErroredState, setGeoMetricsErroredState] = useState(0)
    const LOCALISATION_LOCALES = ["en-GB", "en-AU", "en-NZ", "en-IE"]
    const toggleSettingsModal = () => {
        setTimeout(() => {
            document.getElementById("settingsModalDialog").scrollIntoView()
        }, settingsModal? 250 : 5)
        setSettingsModal(!settingsModal)
    }

    const onAppearanceChange = (e) => {
        setActiveAppearance(settingsMgr.get("appearance_actual"))
    }

    const handleSettingsTabClick = (value) => {
        if (value === activeSettingsTab) {
            return
        }

        setActiveSettingsTab(value)
    }

    const rippleColor = () => {
        if (activeAppearance === "light") {
            return "primary"
        } else if (activeAppearance === "dark") {
            return "light"
        }
    }

    const onGeoMetricsScaffolded = (e) => {
        setGeoMetricsErroredState(props.geoMetrics.geoAPIerrored)
    }

    const renderLocalization = () => {
        if (LOCALISATION_LOCALES.indexOf(navigator.language.toLowerCase()) !== -1) {
            return "Localisation"
        } else {
            return "Localization"
        }
    }

    useEffect(() => {
        document.addEventListener("appearanceChanged", onAppearanceChange.bind(this))
        document.addEventListener("geoAPIScaffolded", onGeoMetricsScaffolded.bind(this))
        return () => {
            document.removeEventListener("appearanceChanged", onAppearanceChange.bind(this))
            document.removeEventListener("geoAPIScaffolded", onGeoMetricsScaffolded.bind(this))
        }
    })

    return (
        <>
        <MDBBtn onClick={toggleSettingsModal} style={{ marginTop: "8px", pointerEvents: "auto" }} title="Click to change tracker preferences">
            <SettingsIcon fontSize="small"></SettingsIcon>
        </MDBBtn><br></br>
        <MDBModal id="settingsModal" appendToBody show={settingsModal} setShow={setSettingsModal} tabIndex='-1'>
            <MDBModalDialog id="settingsModalDialog" className="settingsModalDialog">
                <MDBModalContent>
                <MDBModalHeader>
                    <MDBModalTitle>Tracker Settings</MDBModalTitle>
                    <MDBBtn className='btn-close' color='none' onClick={toggleSettingsModal} title="Click to close this modal"></MDBBtn>
                </MDBModalHeader>
                <MDBModalBody>
                    <MDBRow>
                        <div className='col-sm-4 col-md-3'>
                            <MDBListGroup light style={{ marginBottom: "10px" }}>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'tracker'} onClick={() => handleSettingsTabClick('tracker')} aria-current='true' className='px-3' title="Click to change tracker settings">
                                        <TrackChangesIcon fontSize="small" className="settings-icon"/>Tracker
                                    </MDBListGroupItem>
                                </MDBRipple>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'map'} onClick={() => handleSettingsTabClick('map')} className='px-3' title="Click to change map settings">
                                        <MapIcon fontSize="small" className="settings-icon" />Map
                                    </MDBListGroupItem>
                                </MDBRipple>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'metrics'} onClick={() => handleSettingsTabClick('metrics')} className='px-3' title="Click to change metrics settings">
                                        <ShowChartIcon fontSize="small" className="settings-icon" />Metrics
                                    </MDBListGroupItem>
                                </MDBRipple>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'localization'} onClick={() => handleSettingsTabClick('localization')} className='px-3' title='Click to change localization settings'>
                                        <LanguageIcon fontSize="small" className="settings-icon" />{renderLocalization()}
                                    </MDBListGroupItem>
                                </MDBRipple>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'stopInfo'} onClick={() => handleSettingsTabClick('stopInfo')} className='px-3' title="Click to change stop information window settings">
                                        <InfoOutlined fontSize="small" className="settings-icon" />Stop Info
                                    </MDBListGroupItem>
                                </MDBRipple>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'help'} onClick={() => handleSettingsTabClick('help')} className='px-3' title="Click to view tracker help">
                                        <HelpOutline fontSize="small" className="settings-icon" />Help
                                    </MDBListGroupItem>
                                </MDBRipple>
                                <MDBRipple rippleColor={rippleColor()} style={{ borderRadius: "0.5rem" }}>
                                    <MDBListGroupItem tag='button' action noBorders active={activeSettingsTab === 'reset'} onClick={() => handleSettingsTabClick('reset')} className='px-3' title="Click to reset tracker settings">
                                        <RefreshIcon fontSize="small" className="settings-icon" />Reset
                                    </MDBListGroupItem>
                                </MDBRipple>
                            </MDBListGroup>
                        </div>
                        <div className='col-sm-8 col-md-9'>
                            <MDBTabsContent style={{ marginLeft: "10px", marginRight: "10px" }}>
                                <MDBTabsPane show={activeSettingsTab === 'tracker'}>
                                    <TrackerSettings settingsMgr={props.settingsMgr} dataMgr={props.dataMgr} geoMetricsErrorState={geoMetricsErroredState} />
                                </MDBTabsPane>
                                <MDBTabsPane show={activeSettingsTab === 'localization'}>
                                    <LocalizationSettings settingsMgr={props.settingsMgr} dataMgr={props.dataMgr} geoMetrics={props.geoMetrics} />
                                </MDBTabsPane>
                                <MDBTabsPane show={activeSettingsTab === 'map'}>
                                    <MapSettings settingsMgr={props.settingsMgr} dataMgr={props.dataMgr} geoMetricsErrorState={geoMetricsErroredState} />
                                </MDBTabsPane>
                                <MDBTabsPane show={activeSettingsTab === 'stopInfo'}>
                                    <StopInfoSettings settingsMgr={props.settingsMgr} dataMgr={props.dataMgr} geoMetricsErrorState={geoMetricsErroredState} />
                                </MDBTabsPane>
                                <MDBTabsPane show={activeSettingsTab === 'metrics'}>
                                    <MetricsSettings settingsMgr={props.settingsMgr} geoMetricsErrorState={geoMetricsErroredState} />
                                </MDBTabsPane>
                                <MDBTabsPane show={activeSettingsTab === 'reset'}>
                                    <ResetSettings settingsMgr={props.settingsMgr} toggleSettingsModal={toggleSettingsModal} setSettingsModal={setSettingsModal} setActiveSettingsTab={setActiveSettingsTab} geoMetricsErrorState={geoMetricsErroredState} />
                                </MDBTabsPane>
                                <MDBTabsPane show={activeSettingsTab === 'help'}>
                                    <HelpSettings settingsMgr={props.settingsMgr} dataMgr={props.dataMgr} geoMetricsErrorState={geoMetricsErroredState} />
                                </MDBTabsPane>
                            </MDBTabsContent>
                        </div>
                    </MDBRow>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color='secondary' onClick={toggleSettingsModal} title="Click to close this modal">
                        Close
                    </MDBBtn>
                </MDBModalFooter>
                </MDBModalContent>
            </MDBModalDialog>
        </MDBModal>
        </>
    )
}
import { MDBCard, MDBCardBody, MDBCardText, MDBCardTitle } from "mdb-react-ui-kit"
import React from "react"
// We are now using Flag Icons from CDNJS
//import "../../../node_modules/flag-icons/css/flag-icons.min.css";

class LastStopMetric extends React.Component {
    constructor(props) {
        super(props)
        this.dataMgr = props.dataMgr;
        this.settingsMgr = props.settingsMgr;
        this.state = {
            lastStopInfo: this.dataMgr.getLastStopDetails(),
            flagsVisible: this.settingsMgr.settings.arrivalFlags
        }
    }

    onStopStateChange(e) {
        this.setState({lastStopInfo: this.dataMgr.getLastStopDetails()})
    }

    onSettingChange(e) {
        if (e.detail.setting === "arrivalFlags") {
            this.setState({flagsVisible: this.settingsMgr.settings.arrivalFlags})
        }
    }

    renderFlag() {
        if (this.state.flagsVisible && this.state.lastStopInfo.countrycode !== "zz") {
            return (
                <>
                <i className={"fi fi-" + this.state.lastStopInfo.countrycode}></i>&nbsp;&nbsp;
                </>
            )
        }
    }

    renderText() {
        let stopname;
        if (this.state.lastStopInfo.region !== "") {
            stopname = this.state.lastStopInfo.city + ", " + this.state.lastStopInfo.region
        } else {
            stopname = this.state.lastStopInfo.city
        }

        return (
            <>
            <span>
                {this.renderFlag()}{stopname}
            </span>
            </>
        )
    }

    componentDidMount() {
        document.addEventListener("stopDeparture", this.onStopStateChange.bind(this))
        document.addEventListener("settingChanged", this.onSettingChange.bind(this))
    }

    componentWillUnmount() {
        document.removeEventListener("stopDeparture", this.onStopStateChange.bind(this))
        document.removeEventListener("settingChanged", this.onSettingChange.bind(this))
    }

    render() {
        return (
            <>
                <MDBCard className='shadow-3' style={{ pointerEvents: "auto" }}>
                    <MDBCardBody>
                        <MDBCardTitle>
                            Last stop
                        </MDBCardTitle>
                        <MDBCardText>
                            {this.renderText()}
                        </MDBCardText>
                    </MDBCardBody>
                </MDBCard>
            </>
        )
    }
}

export default LastStopMetric
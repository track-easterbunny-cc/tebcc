import os
import glob
import time
import sys

purge_time = int(sys.argv[2]) * 86400 if len(sys.argv) == 3 and sys.argv[1] == "--purge_days" else 604800
js_files = sorted(glob.glob("static/js/*.js"), key=os.path.getmtime)
css_files = sorted(glob.glob("static/css/*.css"), key=os.path.getmtime)

print(f"Most recent JS file: {js_files[0]}")
print(f"Most recent CSS file: {css_files[0]}")
print(f"Found {len(js_files)} bundles in this environment.")


for file in js_files[2:]:
    if os.path.getmtime(file) < time.time() - purge_time:
        print(f"Removing {file}...")
        os.remove(file)
        os.remove(f"{file}.map")
        os.remove(f"{file}.LICENSE.txt")

for file in css_files[2:]:
    if os.path.getmtime(file) < time.time() - purge_time:
        print(f"Removing {file}...")
        os.remove(file)
        os.remove(f"{file}.map")

print("Done!")

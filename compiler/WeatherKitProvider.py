import requests
from configparser import ConfigParser
import time
import jwt
import pytz
from datetime import datetime

class WeatherKitProvider:
    def __init__(self, dryrun):
        self.dryrun = dryrun
        self.config = ConfigParser()
        self.config.read("config.ini")
        keyfile = open(self.config.get("WEATHERKIT", "keyfile"), "r")
        secret = keyfile.read()
        current_time = int(time.time())
        expiry_time = current_time + self.config.getint("WEATHERKIT", "expiry_time")
        payload = {
            "iss": self.config.get("WEATHERKIT", "iss"),
            "iat": current_time,
            "exp": expiry_time,
            "sub": self.config.get("WEATHERKIT", "sub")
        }

        headers = {
            "alg": "ES256",
            "kid": self.config.get("WEATHERKIT", "kid"),
            "id": self.config.get("WEATHERKIT", "iss") + "." + self.config.get("WEATHERKIT", "sub")
        }

        self.token = jwt.encode(payload, secret, algorithm='ES256', headers=headers)
        self.weatherjson = {}

    def unix_to_iso8601(self, ts):
        tz = pytz.timezone("UTC")
        formatted_iso8601 = datetime.fromtimestamp(ts, tz).isoformat()
        formatted_iso8601 = formatted_iso8601.replace("+00:00", "Z")
        return formatted_iso8601

    def make_wk_request(self, url):
        headers_request = {
            "Authorization": f"Bearer {self.token}"
        }

        data = requests.get(url, headers=headers_request)
        if data.status_code != 200:
            print(f"Failed to make request - Status Code {data.status_code}. Retrying.")
            return self.make_wk_request(url)
            
        try:
            return data.json()
        except requests.exceptions.JSONDecodeError:
            print("Failed to make request. JSON decode failure. Retrying.")
            time.sleep(1)
            return self.make_wk_request(url)

    def fetch(self, latitude, longitude, unixarrival, cityname):
        if not self.dryrun:
            self.weatherjson = self.make_wk_request(f"https://weatherkit.apple.com/api/v1/weather/en/{latitude}/{longitude}?dataSets=currentWeather&currentAsOf={self.unix_to_iso8601(unixarrival)}")
            self.weatherjson = self.weatherjson['currentWeather']
            self.weatherjson['temperatureF'] = (self.weatherjson['temperature'] * (9/5)) + 32
            self.weatherjson['icon'] = self.map_icon_code(self.weatherjson['conditionCode'])
            self.weatherjson['conditionCode'] = self.map_condition_code(self.weatherjson['conditionCode'])
            self.weatherjson['temperatureF'] = int(self.weatherjson['temperatureF'])
            self.weatherjson['temperature'] = int(self.weatherjson['temperature'])
        else:
            self.weatherjson = {
                "temperature": 20,
                "temperatureF": 70,
                "conditionCode": "Clear",
                "icon": self.map_icon_code("Clear")
            }

    def map_condition_code(self, condition_code):
        cc_mapping = {
            "Clear": "Clear",
            "Cloudy": "Cloudy",
            "Dust": "Dust",
            "Fog": "Fog",
            "Foggy": "Foggy",
            "Haze": "Haze",
            "MostlyClear": "Mostly Clear",
            "MostlyCloudy": "Mostly Cloudy",
            "PartlyCloudy": "Partly Cloudy",
            "ScatteredThunderstorms": "Scattered Thunderstorms",
            "Smoke": "Smoke",
            "Breezy": "Breezy",
            "Windy": "Windy",
            "Drizzle": "Drizzle",
            "HeavyRain": "Heavy Rain",
            "Rain": "Rain",
            "Showers": "Showers",
            "Flurries": "Flurries",
            "HeavySnow": "Heavy Snow",
            "MixedRainAndSleet": "Mixed Rain & Sleet",
            "MixedRainAndSnow": "Mixed Rain & Snow",
            "MixedRainfall": "Mixed Rainfall",
            "MixedSnowAndSleet": "Mixed Snow & Sleet",
            "ScatteredShowers": "Scattered Showers",
            "ScatteredSnoShowers": "Scattered Snow Showers",
            "Sleet": "Sleet",
            "Snow": "Snow",
            "SnowShowers": "Snow Showers",
            "Blizzard": "Blizzard",
            "BlowingSnow": "Blowing Snow",
            "FreezingDrizzle": "Freezing Drizzle",
            "FreezingRain": "Freezing Rain",
            "Frigid": "Frigid",
            "Hail": "Hail",
            "Hot": "Hot",
            "Hurricane": "Hurricane",
            "IsolatedThunderstorms": "Isolated Thunderstorms",
            "SevereThunderstorm": "Severe Thunderstorm",
            "Thunderstorm": "Thunderstorm",
            "Thunderstorms": "Thunderstorms",
            "Tornado": "Tornado",
            "TropicalStorm": "Tropical Storm"
        }

        return cc_mapping[condition_code]

    def map_icon_code(self, condition_code):
        icon_mapping = {
            "Clear": "wi-night-clear",
            "Cloudy": "wi-cloudy",
            "Dust": "wi-dist",
            "Fog": "wi-fog",
            "Foggy": "wi-fog",
            "Haze": "wi-dust",
            "MostlyClear": "wi-night-alt-cloudy",
            "MostlyCloudy": "wi-night-alt-cloudy",
            "PartlyCloudy": "wi-night-alt-cloudy",
            "ScatteredThunderstorms": "wi-storm-showers",
            "Smoke": "wi-smog",
            "Breezy": "wi-strong-wind",
            "Windy": "wi-strong-wind",
            "Drizzle": "wi-sprinkle",
            "HeavyRain": "wi-rain",
            "Rain": "wi-showers",
            "Showers": "wi-sprinkle",
            "Flurries": "wi-snow",
            "HeavySnow": "wi-snow",
            "MixedRainAndSleet": "wi-rain-mix",
            "MixedRainAndSnow": "wi-rain-mix",
            "MixedRainfall": "wi-rain-mix",
            "MixedSnowAndSleet": "wi-snow",
            "ScatteredShowers": "wi-sprinkle",
            "ScatteredSnowShowers": "wi-snow",
            "Sleet": "wi-sleet",
            "Snow": "wi-snow",
            "SnowShowers": "wi-snow",
            "Blizzard": "wi-snow-wind",
            "BlowingSnow": "wi-snow-wind",
            "FreezingDrizzle": "wi-sleet",
            "FreezingRain": "wi-sleet",
            "Frigid": "wi-snowflake-cold",
            "Hail": "wi-hail",
            "Hot": "wi-hot",
            "Hurricane": "wi-hurricane",
            "IsolatedThunderstorms": "wi-storm-showers",
            "SevereThunderstorm": "wi-thunderstorm",
            "Thunderstorm": "wi-thunderstorm",
            "Thunderstorms": "wi-thunderstorm",
            "Tornado": "wi-tornado",
            "TropicalStorm": "wi-hurricane"
        }

        return icon_mapping[condition_code]
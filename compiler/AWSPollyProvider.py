from configparser import ConfigParser
import boto3
import mutagen
from mutagen.easyid3 import EasyID3

class AWSPollyProvider:
    def __init__(self, configfile: ConfigParser):
        self.locales = ["en-US", "en-AU", "en-GB", "en-IE", "en-NZ", "en-ZA"]
        self.locales_to_names = {
            "en-US": "Matthew",
            "en-AU": "Olivia",
            "en-GB": "Brian",
            "en-IE": "Niamh",
            "en-NZ": "Aria",
            "en-ZA": "Ayanda"
        }
        self.configfile = configfile

        self.polly_client = boto3.Session(
            aws_access_key_id=self.configfile.get("POLLY", "access_key"),
            aws_secret_access_key=self.configfile.get("POLLY", "secret_key"),
            region_name=self.configfile.get("POLLY", "region")
        ).client("polly")

    def fetch(self, locale, text, hashsum):
        text = text.replace('"', "&quot;").replace("&", "&amp;").replace("'", "&apos;").replace("<", "&lt;").replace(">", "&gt;")
        text = text.split(",")
        final_text = "<speak>"
        for text_segment in text:
            final_text += text_segment + "<break strength='medium' />,"

        final_text += "<break time='0.25s' /></speak>"
        file_path = f"audio/{locale}/{hashsum}.mp3"
        response = self.polly_client.synthesize_speech(
            VoiceId=self.locales_to_names[locale],
            OutputFormat='mp3',
            Text=final_text,
            TextType='ssml',
            Engine='neural'
        )

        file = open(file_path, 'wb')
        file.write(response['AudioStream'].read())
        file.close()

        # Modify MP3 tags
        try:
            meta = EasyID3(file_path)
        except:
            meta = mutagen.File(file_path, easy=True)
            meta.add_tags()

        meta['title'] = text
        meta['artist'] = "track.easterbunny.cc"
        meta.save(v1=2)
        
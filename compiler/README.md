# TEBCC Route Compiler (v3)
This is the series of scripts that does our route compilation.

# Route compilation? What's that?
Yes, the Easter Bunny Tracker uses a route. We do not have satellites orbiting the earth and use whatever I explain in the FAQ. It is just a .json file telling the tracker where the bunny needs to go next.

However, this route lives in Google Sheets so it's much easier for me to edit and work on it. This compiler takes a .tsv output from sheets (.csv has hurt me before so I swear by .tsv) and then converts it into a JSON file the tracker can read.

# Backwards compatibility note
v3 of the route compiler generates routes that are backwards compatibile with v5.5 with one exception - you have to slightly change the weather icon class prefixes so it's `wi wi-{the value in the route}` rather than `wi wi-darksky-` or whatever it was. Just keep that in mind.

# Setup
The compiler requires Python 3.8 or higher with pip.

To setup, run `pip3 install -r requirements.txt`. You will also need to do this with the WeatherKitProvider, do the same command in there as well.

## A note about WeatherKitProvider
WKP is an internal library I built as a lot of my projects had to migrate from Dark Sky to WeatherKit, and this library helps make interacting with WeatherKit easier.

This library is NOT meant to be used in other projects and you really shouldn't use it anyway. It's purely designed for internal use and as such there's almost zero error catching in the library and I don't know what happens when you put it in edge cases. One day I might open-source it, but that day is not here

In short: don't use the WKP library because you will get hurt trying to use it.

# Keys keys keys
The compiler requires a Google Maps Timezone API key, WeatherKit access, AWS keys for AWS Polly, and a GeoNames account. GeoNames is free, and for AWS Polly you should be able to sign up for an account and any usage will fall in the free tier.

With WeatherKit, you'll want to generate an App ID that has access to WeatherKit in both the App and Service ID. You also need to generate a signing key with a .p8 file.

Make sure you put that .p8 file in the base directory of the compiler, and specify necessary options in config.ini.

## Don't want to use WeatherKit?
You don't have to and it's understandable - a dev membership is $99/yr. But you have two options here.

First, you can run the tracker strictly with dry runs which avoids any WKP calls. you'll probably need to modify the WKP so that the __init__ in WKP doesn't read the .p8 file and then you're fine.

Second, you could switch to a different weather provider. Just make sure of a few things:
* You need to be able to cleanly get a weather conditions string
* You need to get a temperature
* You need to somehow translate whatever iconography your weather provider does into some weather icons class.

## Don't want to use AWS Polly?
You don't have to and can disable it in the compiler. However, you will run into errors when attempting to load stop pronunciations in Extended Stop Data.

# The route file
v3 of the compiler depends on v6 of the TEBCC route format. v5 route formats are NOT compatible.

For more information, please read ROUTE.md to get a sense of how routes are structured. There are a lot of rows. If you are too lazy to make a route file, I've included 2023's route file in the repository (`route.tsv`).

Just note that I used custom formulas to get the baskets delivered/carrots eaten and that didn't carry over to the TSV file. Baskets delivered is calculated by multiplying population by like 4.3 (+/- a bit) in addition to an always present baskets value (like 60-80 w/ variation) so stops with super low populations still have some action, then the previous row is summed up so it's VERY hard to go backwards.

Carrots eaten is baskets delivered divided by 125 with some variation (but not too much). too much variation = sometimes the count can go backwards.

# The config file
Most of the operation occurs in `config.ini`. Therefore you need to carefully read what all the sections and options do. It also has example values that you can use to get started.

For detailed documentation about what each key/pair value does (and the type expected), read the `CONFIG.md` file.

# Using the compiler
First, make sure your route is in a file called `route.tsv` and that this route follows the route spec. You can modify the filename in `compile.py` if you'd like.

Simply run `python3 compile.py`. Checks are automatically done at startup. 

If you have errors in your data, the compiler will let you know and not compile the route. There's a lot of checks and they're pretty descriptive (along with telling you which line they occurred on), so just pay attention to that.

There's also warnings in the data, which mostly fall into these categories:
* High amounts of latitude/longitude variations (it's normal to see these when the bunny crosses the International Date Line)
* Setting the population year to `0` (which will disable the population year in Extended Stop Data)
* And some other checks you can see by reading the source code.

If there are any warnings/errors, the compiler will let you know which row they occur on and what the stop name is for that row.

Give the compiler time. For dryrun routes, compilation takes a few seconds. When you start enabling online services, compilation runs can take anywhere from 5-20 minutes.

At the end of the process, two files are outputted. `route.json` is the file the tracker will use to read the route, make sure you put that in the right directory (`/static/routes`). `route_compiled.tsv` is a TSV file that, if the compiler needed to request timezone/wikipedia/locale data, includes those fields filled out.

You need to watch out when using GeoNames locale data. GeoNames will rate limit you to 1,000 requests in a time period (i think an hour or so?), so if your route goes past 1,000 rows and needs locales the compiler will fail. Thankfully `route_compiled.tsv` is written to after every row, so if you transplant the locales column from compiled into the route.tsv file then you'll be all good and the next compilation will start where it left off. DONT RENAME IT HOWEVER - as it'll only be partially filled out and then you lose the remaining rows in your route!

This is the case for almost all online services (timezone/wikipedia) assuming force fetch is off - data is only fetched if there's nothing there.

When AWS Polly fetching is enabled, it will start to fill up the compiler/audio folder with the appropriate audio files if it doesn't exist. Make sure that if you change stop names/regions that you regenerate audio files.

Also, just a quick note - if you delete a stop, the old stop pronunciation won't automatically get deleted. The best way to get around this is that whenever you're about to go into production, delete all the audio files and regenerate them. It takes a lot of TTS to get AWS Polly to start charging you!

# How the whole thing works
In short:

```commandline
run compile.py -> checks if TSV is valid -> fetches DS/Wikipedia/TZ for each line -> compiles into JSON -> outputs modified TSV for back to Google Sheets
```

The GitLab CI pipeline takes care of uploading to prod/nonprod depending on which branch is used.

# Next steps
I've included the route for 2024 inside this folder that I used. I recommend you use this as a starting point for your route. Additionally - read up on the `ROUTE.md` file, as this describes in detail how the route table needs to be formatted.
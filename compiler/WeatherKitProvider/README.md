# WeatherKitProvider
WeatherKitProvider is an internal library that I made for many projects to wrap Apple's WeatherKit API. It is a core component of the tracker given the compiler relies on it to make weather requests.

However, this library is not intended to be used publicly. Please do not try to use this library in your projects, you will be bitten by it.
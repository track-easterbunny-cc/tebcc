# This is a translation table of WeatherKit Precipitation Codes -> is it precip?
wkprecip_to_precipitable = {
    "clear": False,
    "precipitation": True,
    "rain": True,
    "snow": True,
    "sleet": True,
    "hail": True,
    "mixed": True
}
# This is a translation table of WeatherKit precipitation codes -> Human readable codes.
wkprecip_to_human = {
    "clear": "Clear",
    "precipitation": "Precipitation",
    "rain": "Rain",
    "snow": "Snow",
    "sleet": "Sleet",
    "hail": "Hail",
    "mixed": "Mixed precipitation"
}
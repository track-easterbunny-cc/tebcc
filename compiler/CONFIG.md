## COMPILER section

### starttime
This is the reference time that the compiler uses for when the route actually starts, so make sure this is equal to row 1 of the route. UNIX timestamp.

### actualruntime
If you'd like a run to occur at an offset, set the starttime here. Again, this corresponds to what you want it to be at row 1 of the route. UNIX timestamp.

### basestop_cityname
The name of the first and last stop of tracking. By default, it's `Easter Bunny's Workshop`. Your first and last stop of tracking MUST match this for the tracker to work properly, I couldn't tell you what happens if this is wrong (probably the tracker gets stuck in a bad state).

### override_auto_devstart
Boolean to indicate whether or not if you're running the compiler in CI/CD that the automatic dev start time (for the current day) is overriden or not.

### override_auto_mainstart
Boolean to indicate whether or not if you're running the compiler in CI/CD that the automatic main start time (which is...starttime) is overriden or not.

## WEATHERKIT section
This entire section controls how WeatherKit is used.

### keyfile
The name of the keyfile in the base of the compiler directory. Include the extension as well.

### expiry_time
The amount of time in seconds for how long the auth token expires when the compiler is fired up. 3600 seconds is usually plenty long enough.

### iss
This is your Apple Developer's account Team ID.

### sub
This is the identifier for the App ID that you made earlier for WeatherKit.

### kid
This is your auth key's ID. For example, a file with AuthKey_1234ABCD.p8 would have a kid of `1234ABCD`.

### dryrun_dev
Whether or not you're doing a dry run of weather data in the dev environment. Because depending on your route any compilation can throw up a run of WK calls, you can enable a dry run and make the weather data 70 degrees and clear for ESD.

### dryrun_staging
Same as `dryrun_dev`, but for the staging environment. Should be False in a properly deployed TEBCC environment.

### dryrun_prod
Same as `dryrun_prod`, but for the production environment. Should be False in a properly deployed TEBCC environment.

## TZ section
This entire section controls how TZ fetching is done in the compiler

### use
Whether or not the compiler will use the Google Timezone API to fetch a timezone for any timezone data fields that are not populated.

### force_fetch
Whether or not to forcibly fetch timezone data even if timezone data is present in fields.

### apikey
Self explanatory.

### processingtime
UNIX timestamp for the reference for the timezone data. Should be the start time of the route.

## WIKIPEDIA section
This section controls how Wikipedia is used within the compiler

### use
Whether or not the compiler will automatically fetch Wikipedia descriptions for stops that do not have a Wikipedia description field.

### force_fetch
Whether or not the compiler will fetch Wikipedia descriptions for all stops, including ones that already have a Wikipedia description field filled out.

### cleanup
Whether or not to clean up the data returned from Wikipedia. It is strongly recommended to turn this on as we automatically clean up any reference tags, remove pronunciation (with a 99% success rate), in addition to any random crap we've seen before.

## GEONAMES section
This section controls how GeoNames is used within the compiler (which is used to get locales for each stop).

### use
Whether or not the compiler will automatically fetch GeoNames locale data for stops that do not have a locale field filled out.

### force_fetch
Whether or not the compiler will fetch GeoNames locale data for all stops, including ones that already have a GeoNames locale field filled out.

Note that if you enable this and have a long route with over 1,000 stops that you will run into GeoNames rate limits, so BE CAREFUL enabling this field. More info in the next section.

### username
Your account username for GeoNames.

## TOBCC
This section controls specific controls for our sister site, track.orthodoxbunny.cc

### tobcc_mode
Boolean to control if TOBCC mode is enabled in the compiler. In short, this lowers basket delivered and carrot eaten counts by 5x for TOBCC.

## POLLY
This section controls specific settings for accessing AWS Polly in the compiler.

### access_key
The AWS access key to use when accessing AWS Polly.

### secret_key
The AWS secret key to use when accessing AWS Polly.

### region
The AWS region to be used when accessing AWS Polly. You can usually leave this set to `us-east-1`.

### use
Whether or not the compiler will automatically fetch AWS Polly files, if they don't already exist in the `compiler/audio` folder. If a file doesn't exist for a specific stop (which is the stop & region hashed as MD5), then the compiler will fetch the audio file.

### force_fetch
Whether or not the compiler will fetch AWS Polly files even for stops that already have an audio file attached to it.